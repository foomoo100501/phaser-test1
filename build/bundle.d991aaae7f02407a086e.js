/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, { enumerable: true, get: getter });
/******/ 		}
/******/ 	};
/******/
/******/ 	// define __esModule on exports
/******/ 	__webpack_require__.r = function(exports) {
/******/ 		if(typeof Symbol !== 'undefined' && Symbol.toStringTag) {
/******/ 			Object.defineProperty(exports, Symbol.toStringTag, { value: 'Module' });
/******/ 		}
/******/ 		Object.defineProperty(exports, '__esModule', { value: true });
/******/ 	};
/******/
/******/ 	// create a fake namespace object
/******/ 	// mode & 1: value is a module id, require it
/******/ 	// mode & 2: merge all properties of value into the ns
/******/ 	// mode & 4: return value when already ns object
/******/ 	// mode & 8|1: behave like require
/******/ 	__webpack_require__.t = function(value, mode) {
/******/ 		if(mode & 1) value = __webpack_require__(value);
/******/ 		if(mode & 8) return value;
/******/ 		if((mode & 4) && typeof value === 'object' && value && value.__esModule) return value;
/******/ 		var ns = Object.create(null);
/******/ 		__webpack_require__.r(ns);
/******/ 		Object.defineProperty(ns, 'default', { enumerable: true, value: value });
/******/ 		if(mode & 2 && typeof value != 'string') for(var key in value) __webpack_require__.d(ns, key, function(key) { return value[key]; }.bind(null, key));
/******/ 		return ns;
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "";
/******/
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = "./src/game.ts");
/******/ })
/************************************************************************/
/******/ ({

/***/ "./src/game.ts":
/*!*********************!*\
  !*** ./src/game.ts ***!
  \*********************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";
eval("\nObject.defineProperty(exports, \"__esModule\", { value: true });\nconst GameScene_1 = __webpack_require__(/*! ./scenes/GameScene */ \"./src/scenes/GameScene.ts\");\nclass Game extends Phaser.Game {\n    constructor(config) {\n        super(config);\n    }\n}\nconst gameConfig = {\n    backgroundColor: \"#3A99D9\",\n    scene: [GameScene_1.GameScene],\n    title: \"Test Project\",\n    type: Phaser.WEBGL,\n    width: 800,\n    height: 600,\n    render: {\n        pixelArt: true,\n    },\n    input: {\n        keyboard: true,\n    },\n};\nwindow.onload = () => {\n    const game = new Game(gameConfig);\n};\n//# sourceURL=[module]\n//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiLi9zcmMvZ2FtZS50cy5qcyIsInNvdXJjZXMiOlsid2VicGFjazovLy8uL3NyYy9nYW1lLnRzP2FhZmMiXSwic291cmNlc0NvbnRlbnQiOlsiXCJ1c2Ugc3RyaWN0XCI7XG5PYmplY3QuZGVmaW5lUHJvcGVydHkoZXhwb3J0cywgXCJfX2VzTW9kdWxlXCIsIHsgdmFsdWU6IHRydWUgfSk7XG5jb25zdCBHYW1lU2NlbmVfMSA9IHJlcXVpcmUoXCIuL3NjZW5lcy9HYW1lU2NlbmVcIik7XG5jbGFzcyBHYW1lIGV4dGVuZHMgUGhhc2VyLkdhbWUge1xuICAgIGNvbnN0cnVjdG9yKGNvbmZpZykge1xuICAgICAgICBzdXBlcihjb25maWcpO1xuICAgIH1cbn1cbmNvbnN0IGdhbWVDb25maWcgPSB7XG4gICAgYmFja2dyb3VuZENvbG9yOiBcIiMzQTk5RDlcIixcbiAgICBzY2VuZTogW0dhbWVTY2VuZV8xLkdhbWVTY2VuZV0sXG4gICAgdGl0bGU6IFwiVGVzdCBQcm9qZWN0XCIsXG4gICAgdHlwZTogUGhhc2VyLldFQkdMLFxuICAgIHdpZHRoOiA4MDAsXG4gICAgaGVpZ2h0OiA2MDAsXG4gICAgcmVuZGVyOiB7XG4gICAgICAgIHBpeGVsQXJ0OiB0cnVlLFxuICAgIH0sXG4gICAgaW5wdXQ6IHtcbiAgICAgICAga2V5Ym9hcmQ6IHRydWUsXG4gICAgfSxcbn07XG53aW5kb3cub25sb2FkID0gKCkgPT4ge1xuICAgIGNvbnN0IGdhbWUgPSBuZXcgR2FtZShnYW1lQ29uZmlnKTtcbn07XG4iXSwibWFwcGluZ3MiOiJBQUFBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOyIsInNvdXJjZVJvb3QiOiIifQ==\n//# sourceURL=webpack-internal:///./src/game.ts\n");

/***/ }),

/***/ "./src/objects/Player.ts":
/*!*******************************!*\
  !*** ./src/objects/Player.ts ***!
  \*******************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";
eval("\nObject.defineProperty(exports, \"__esModule\", { value: true });\nclass Player extends Phaser.GameObjects.Image {\n    constructor(scene, x, y) {\n        super(scene, x, y, \"player\");\n        this.setOrigin(0.5, 0.5);\n        this.setScale(2, 2);\n        this.scene.add.existing(this);\n        this._cursor = this.scene.input.keyboard.createCursorKeys();\n        this._direction = new Phaser.Math.Vector2();\n    }\n    update() {\n        this.handleInput();\n    }\n    handleInput() {\n        if (this._cursor.right.isDown) {\n            this._direction.x = 1;\n        }\n        else if (this._cursor.left.isDown) {\n            this._direction.x = -1;\n        }\n        else {\n            this._direction.x = 0;\n        }\n        if (this._cursor.up.isDown) {\n            this._direction.y = -1;\n        }\n        else if (this._cursor.down.isDown) {\n            this._direction.y = 1;\n        }\n        else {\n            this._direction.y = 0;\n        }\n        if (this._direction.x !== 0 || this._direction.y !== 0) {\n            this._direction.normalize();\n            this.x += this._direction.x * 5;\n            this.y += this._direction.y * 5;\n        }\n    }\n}\nexports.Player = Player;\n//# sourceURL=[module]\n//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiLi9zcmMvb2JqZWN0cy9QbGF5ZXIudHMuanMiLCJzb3VyY2VzIjpbIndlYnBhY2s6Ly8vLi9zcmMvb2JqZWN0cy9QbGF5ZXIudHM/MWNmOSJdLCJzb3VyY2VzQ29udGVudCI6WyJcInVzZSBzdHJpY3RcIjtcbk9iamVjdC5kZWZpbmVQcm9wZXJ0eShleHBvcnRzLCBcIl9fZXNNb2R1bGVcIiwgeyB2YWx1ZTogdHJ1ZSB9KTtcbmNsYXNzIFBsYXllciBleHRlbmRzIFBoYXNlci5HYW1lT2JqZWN0cy5JbWFnZSB7XG4gICAgY29uc3RydWN0b3Ioc2NlbmUsIHgsIHkpIHtcbiAgICAgICAgc3VwZXIoc2NlbmUsIHgsIHksIFwicGxheWVyXCIpO1xuICAgICAgICB0aGlzLnNldE9yaWdpbigwLjUsIDAuNSk7XG4gICAgICAgIHRoaXMuc2V0U2NhbGUoMiwgMik7XG4gICAgICAgIHRoaXMuc2NlbmUuYWRkLmV4aXN0aW5nKHRoaXMpO1xuICAgICAgICB0aGlzLl9jdXJzb3IgPSB0aGlzLnNjZW5lLmlucHV0LmtleWJvYXJkLmNyZWF0ZUN1cnNvcktleXMoKTtcbiAgICAgICAgdGhpcy5fZGlyZWN0aW9uID0gbmV3IFBoYXNlci5NYXRoLlZlY3RvcjIoKTtcbiAgICB9XG4gICAgdXBkYXRlKCkge1xuICAgICAgICB0aGlzLmhhbmRsZUlucHV0KCk7XG4gICAgfVxuICAgIGhhbmRsZUlucHV0KCkge1xuICAgICAgICBpZiAodGhpcy5fY3Vyc29yLnJpZ2h0LmlzRG93bikge1xuICAgICAgICAgICAgdGhpcy5fZGlyZWN0aW9uLnggPSAxO1xuICAgICAgICB9XG4gICAgICAgIGVsc2UgaWYgKHRoaXMuX2N1cnNvci5sZWZ0LmlzRG93bikge1xuICAgICAgICAgICAgdGhpcy5fZGlyZWN0aW9uLnggPSAtMTtcbiAgICAgICAgfVxuICAgICAgICBlbHNlIHtcbiAgICAgICAgICAgIHRoaXMuX2RpcmVjdGlvbi54ID0gMDtcbiAgICAgICAgfVxuICAgICAgICBpZiAodGhpcy5fY3Vyc29yLnVwLmlzRG93bikge1xuICAgICAgICAgICAgdGhpcy5fZGlyZWN0aW9uLnkgPSAtMTtcbiAgICAgICAgfVxuICAgICAgICBlbHNlIGlmICh0aGlzLl9jdXJzb3IuZG93bi5pc0Rvd24pIHtcbiAgICAgICAgICAgIHRoaXMuX2RpcmVjdGlvbi55ID0gMTtcbiAgICAgICAgfVxuICAgICAgICBlbHNlIHtcbiAgICAgICAgICAgIHRoaXMuX2RpcmVjdGlvbi55ID0gMDtcbiAgICAgICAgfVxuICAgICAgICBpZiAodGhpcy5fZGlyZWN0aW9uLnggIT09IDAgfHwgdGhpcy5fZGlyZWN0aW9uLnkgIT09IDApIHtcbiAgICAgICAgICAgIHRoaXMuX2RpcmVjdGlvbi5ub3JtYWxpemUoKTtcbiAgICAgICAgICAgIHRoaXMueCArPSB0aGlzLl9kaXJlY3Rpb24ueCAqIDU7XG4gICAgICAgICAgICB0aGlzLnkgKz0gdGhpcy5fZGlyZWN0aW9uLnkgKiA1O1xuICAgICAgICB9XG4gICAgfVxufVxuZXhwb3J0cy5QbGF5ZXIgPSBQbGF5ZXI7XG4iXSwibWFwcGluZ3MiOiJBQUFBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7Iiwic291cmNlUm9vdCI6IiJ9\n//# sourceURL=webpack-internal:///./src/objects/Player.ts\n");

/***/ }),

/***/ "./src/objects/Star.ts":
/*!*****************************!*\
  !*** ./src/objects/Star.ts ***!
  \*****************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";
eval("\nObject.defineProperty(exports, \"__esModule\", { value: true });\nclass Star extends Phaser.GameObjects.Image {\n    constructor(scene, x, y) {\n        super(scene, x, y, \"star\");\n        this.setOrigin(0.5, 0.5);\n        this.setScale(3, 3);\n        this.scene.add.existing(this);\n    }\n    changePosition() {\n        this.x = Phaser.Math.RND.integerInRange(100, 500);\n        this.y = Phaser.Math.RND.integerInRange(100, 500);\n    }\n}\nexports.Star = Star;\n//# sourceURL=[module]\n//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiLi9zcmMvb2JqZWN0cy9TdGFyLnRzLmpzIiwic291cmNlcyI6WyJ3ZWJwYWNrOi8vLy4vc3JjL29iamVjdHMvU3Rhci50cz8zMWY4Il0sInNvdXJjZXNDb250ZW50IjpbIlwidXNlIHN0cmljdFwiO1xuT2JqZWN0LmRlZmluZVByb3BlcnR5KGV4cG9ydHMsIFwiX19lc01vZHVsZVwiLCB7IHZhbHVlOiB0cnVlIH0pO1xuY2xhc3MgU3RhciBleHRlbmRzIFBoYXNlci5HYW1lT2JqZWN0cy5JbWFnZSB7XG4gICAgY29uc3RydWN0b3Ioc2NlbmUsIHgsIHkpIHtcbiAgICAgICAgc3VwZXIoc2NlbmUsIHgsIHksIFwic3RhclwiKTtcbiAgICAgICAgdGhpcy5zZXRPcmlnaW4oMC41LCAwLjUpO1xuICAgICAgICB0aGlzLnNldFNjYWxlKDMsIDMpO1xuICAgICAgICB0aGlzLnNjZW5lLmFkZC5leGlzdGluZyh0aGlzKTtcbiAgICB9XG4gICAgY2hhbmdlUG9zaXRpb24oKSB7XG4gICAgICAgIHRoaXMueCA9IFBoYXNlci5NYXRoLlJORC5pbnRlZ2VySW5SYW5nZSgxMDAsIDUwMCk7XG4gICAgICAgIHRoaXMueSA9IFBoYXNlci5NYXRoLlJORC5pbnRlZ2VySW5SYW5nZSgxMDAsIDUwMCk7XG4gICAgfVxufVxuZXhwb3J0cy5TdGFyID0gU3RhcjtcbiJdLCJtYXBwaW5ncyI6IkFBQUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOyIsInNvdXJjZVJvb3QiOiIifQ==\n//# sourceURL=webpack-internal:///./src/objects/Star.ts\n");

/***/ }),

/***/ "./src/scenes/GameScene.ts":
/*!*********************************!*\
  !*** ./src/scenes/GameScene.ts ***!
  \*********************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";
eval("\nObject.defineProperty(exports, \"__esModule\", { value: true });\nconst Player_1 = __webpack_require__(/*! ../objects/Player */ \"./src/objects/Player.ts\");\nconst Star_1 = __webpack_require__(/*! ../objects/Star */ \"./src/objects/Star.ts\");\nclass GameScene extends Phaser.Scene {\n    constructor() {\n        super({ key: \"GameScene\" });\n        this._counter = 0;\n    }\n    preload() {\n        this.load.image(\"player\", \"../assets/player.png\");\n        this.load.image(\"star\", \"../assets/coin.png\");\n    }\n    create() {\n        this._player = new Player_1.Player(this, 32, 32);\n        this._star = new Star_1.Star(this, 100, 100);\n        const style = {\n            fontFamily: \"Roboto\",\n            fontSize: 28,\n            fill: \"#ffffff\",\n        };\n        this._counterText = this.add.text(10, this.sys.canvas.height - 50, this._counter.toString(), style);\n    }\n    update() {\n        this._player.update();\n        this._star.update();\n        const intersection = Phaser.Geom.Intersects.RectangleToRectangle(this._player.getBounds(), this._star.getBounds());\n        if (intersection) {\n            this.updateCoinStatus();\n        }\n    }\n    updateCoinStatus() {\n        this._counter++;\n        this._star.changePosition();\n        this._counterText.setText(this._counter.toString());\n    }\n}\nexports.GameScene = GameScene;\n//# sourceURL=[module]\n//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiLi9zcmMvc2NlbmVzL0dhbWVTY2VuZS50cy5qcyIsInNvdXJjZXMiOlsid2VicGFjazovLy8uL3NyYy9zY2VuZXMvR2FtZVNjZW5lLnRzP2VlMWEiXSwic291cmNlc0NvbnRlbnQiOlsiXCJ1c2Ugc3RyaWN0XCI7XG5PYmplY3QuZGVmaW5lUHJvcGVydHkoZXhwb3J0cywgXCJfX2VzTW9kdWxlXCIsIHsgdmFsdWU6IHRydWUgfSk7XG5jb25zdCBQbGF5ZXJfMSA9IHJlcXVpcmUoXCIuLi9vYmplY3RzL1BsYXllclwiKTtcbmNvbnN0IFN0YXJfMSA9IHJlcXVpcmUoXCIuLi9vYmplY3RzL1N0YXJcIik7XG5jbGFzcyBHYW1lU2NlbmUgZXh0ZW5kcyBQaGFzZXIuU2NlbmUge1xuICAgIGNvbnN0cnVjdG9yKCkge1xuICAgICAgICBzdXBlcih7IGtleTogXCJHYW1lU2NlbmVcIiB9KTtcbiAgICAgICAgdGhpcy5fY291bnRlciA9IDA7XG4gICAgfVxuICAgIHByZWxvYWQoKSB7XG4gICAgICAgIHRoaXMubG9hZC5pbWFnZShcInBsYXllclwiLCBcIi4uL2Fzc2V0cy9wbGF5ZXIucG5nXCIpO1xuICAgICAgICB0aGlzLmxvYWQuaW1hZ2UoXCJzdGFyXCIsIFwiLi4vYXNzZXRzL2NvaW4ucG5nXCIpO1xuICAgIH1cbiAgICBjcmVhdGUoKSB7XG4gICAgICAgIHRoaXMuX3BsYXllciA9IG5ldyBQbGF5ZXJfMS5QbGF5ZXIodGhpcywgMzIsIDMyKTtcbiAgICAgICAgdGhpcy5fc3RhciA9IG5ldyBTdGFyXzEuU3Rhcih0aGlzLCAxMDAsIDEwMCk7XG4gICAgICAgIGNvbnN0IHN0eWxlID0ge1xuICAgICAgICAgICAgZm9udEZhbWlseTogXCJSb2JvdG9cIixcbiAgICAgICAgICAgIGZvbnRTaXplOiAyOCxcbiAgICAgICAgICAgIGZpbGw6IFwiI2ZmZmZmZlwiLFxuICAgICAgICB9O1xuICAgICAgICB0aGlzLl9jb3VudGVyVGV4dCA9IHRoaXMuYWRkLnRleHQoMTAsIHRoaXMuc3lzLmNhbnZhcy5oZWlnaHQgLSA1MCwgdGhpcy5fY291bnRlci50b1N0cmluZygpLCBzdHlsZSk7XG4gICAgfVxuICAgIHVwZGF0ZSgpIHtcbiAgICAgICAgdGhpcy5fcGxheWVyLnVwZGF0ZSgpO1xuICAgICAgICB0aGlzLl9zdGFyLnVwZGF0ZSgpO1xuICAgICAgICBjb25zdCBpbnRlcnNlY3Rpb24gPSBQaGFzZXIuR2VvbS5JbnRlcnNlY3RzLlJlY3RhbmdsZVRvUmVjdGFuZ2xlKHRoaXMuX3BsYXllci5nZXRCb3VuZHMoKSwgdGhpcy5fc3Rhci5nZXRCb3VuZHMoKSk7XG4gICAgICAgIGlmIChpbnRlcnNlY3Rpb24pIHtcbiAgICAgICAgICAgIHRoaXMudXBkYXRlQ29pblN0YXR1cygpO1xuICAgICAgICB9XG4gICAgfVxuICAgIHVwZGF0ZUNvaW5TdGF0dXMoKSB7XG4gICAgICAgIHRoaXMuX2NvdW50ZXIrKztcbiAgICAgICAgdGhpcy5fc3Rhci5jaGFuZ2VQb3NpdGlvbigpO1xuICAgICAgICB0aGlzLl9jb3VudGVyVGV4dC5zZXRUZXh0KHRoaXMuX2NvdW50ZXIudG9TdHJpbmcoKSk7XG4gICAgfVxufVxuZXhwb3J0cy5HYW1lU2NlbmUgPSBHYW1lU2NlbmU7XG4iXSwibWFwcGluZ3MiOiJBQUFBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7Iiwic291cmNlUm9vdCI6IiJ9\n//# sourceURL=webpack-internal:///./src/scenes/GameScene.ts\n");

/***/ })

/******/ });